**Flat:**

1. Parameter: depth (number) optional(by default it is 1)
2. Return: A single array consisting of arrays flattened to the required depth.
   
3. Examples:

    const arr1 = [1,2,[3,4],[[5,6]]];
    console.log(arr1.flat()); // [ 1, 2, 3, 4, [ 5, 6 ] ]

    const arr1 = [1,[2,[3,4,[5,6]]],[7,[8]]];
    console.log(arr1.flat(3)); //[1, 2, 3, 4, 5, 6, 7, 8]

    const arr1 = [1,[2,[3,4,[5,6]]],[7,[8]]];
    console.log(arr1.flat()); //[ 1, 2, [ 3, 4, [ 5, 6 ] ], 7, [ 8 ] ]

4. flat accepts depth as input and flatten the array a/c to that depth, be default it flattens the array to depth 1.

5.  No Mutation

**Push:**

1. Parameter: n (any number of elements).
2. Return: length of updated array.
   
3. Examples:

    const arr1 =[1,2,3,4];
    console.log(arr1.push(5,6,8)); //7
    console.log(arr1); //[1, 2, 3, 4,5, 6, 8]

    const arr1 =[1,2,3,4];
    arr1.push([5,6])
    console.log(arr1); //[ 1, 2, 3, 4, [ 5, 6 ] ]

    const arr1 =[1,2,3,4];
    arr1.push([5,[6]]);
    console.log(arr1); //[ 1, 2, 3, 4, [ 5, [ 6 ] ] ]

4. It adds elements at the end of the array.

5. Mutates.
   
**Index of:**

1. Parameter: searchElement (string, numbers, boolean), fromIndex(integer) Optional
2. Return: firstIndex/ -1 (Number)
   
3. Examples:

    const arr1 =[1,2,3,4];
    console.log(arr1.indexOf(3)); //2

    const arr1 =[1,2,3,true, true];
    console.log(arr1.indexOf(true)); //3

    const arr1 = [1,2,3,4,5,2];
    console.log(arr1.indexOf(2, 3)); //5
    
4. It returns first index of the element if found, else -1

5. No Mutation.

**lastIndexOf():**

1. Parameter: searchElement (string, numbers, boolean), fromIndex(integer) Optional
2. Return: lastIndex/ -1 (Number)
   
3. Examples:

    const arr1 =[1,2,3,4];
    console.log(arr1.lastIndexOf(3)); //2

    const arr1 =[1,2,3,true, true];
    console.log(arr1.lastIndexOf(true)); //4

    const arr1 = [1,2,3,4,5,2];
    console.log(arr1.indexOf(2)); //5
    
4. It returns last index of the element if found, else -1

5. No Mutation.

**includes:**

1. Parameter: searchElement (string, numbers, boolean), fromIndex(integer) Optional
2. Return: true/false

3. Examples:

    const arr1 = [1,2,3,4,5,2];
    console.log(arr1.includes(2)); //true

    const arr1 = [1,2,3,4,5,2];
    console.log(arr1.includes(6)); //false

    const arr1 = [1,2,3,4,5,2];
    console.log(arr1.includes(2, 2)); //true
    
4. if the element is found it returs true else false;

5. No Mutation. 

**reverse:**

1. Parameter: No Parameters
2. Return: array

3. Examples:

    const arr1 = [1,2,3,4,5,2];
    console.log(arr1.reverse()); //[ 2, 5, 4, 3, 2, 1 ]

    const arr1 = [1,2,3,4,5,[1,2]];
    console.log(arr1.reverse()); //[ [ 1, 2 ], 5, 4, 3, 2, 1 ]

    const arr1 = [true, false];
    console.log(arr1.reverse()); //[ false, true ]
    
4. reference to reversed array;

5. Mutates.

**every:**

1. Parameter: callback(currentElement, currentIndex, array), thisargument optional 
2. Return: true/false

3. Examples:

    const arr1 = [1,2,3,4,5,6,7];

    function big(element, index, array){
        return element>0;
    }
    console.log(arr1.every(big)); //true

    const isSubset = (array1, array2) =>
        array2.every((element) => array1.includes(element));

    console.log(isSubset([1, 2, 3, 4, 5, 6, 7], [5, 7, 6])); // true
    
4. it tests if all elements pass the given condition and if any element doesnt pass it return false else return true.

5. No Mutation.

**shift:**

1. Parameter: none
2. Return: element that is removed from array/ undefined if array is empty

3. Examples:

    arr = [1,2,4,5];
    console.log(arr.shift()); //1
    console.log(arr); //[2,4,5]

    arr = [];
    console.log(arr.shift()); //undefined
    console.log(arr); //[]

    arr = [6,2,4,5];
    console.log(arr.shift()); //6
    console.log(arr); //[2,4,5]
        
4. it deletes the first element of the array.

5. Mutates.


**splice:**

1. Parameter: start(number), deletecount(number), item-{n}
2. Return: array containing deleted elements

3. Examples:

    arr = [1,2,3,4,5,6]
    console.log(arr.splice(2,4)); // [3,4,5,6]
    console.log(arr); // [1,2]

    arr = [];
    console.log(arr.splice()); //[]
    console.log(arr); //[]

    arr = [6,2,4,5];
    console.log(arr.splice()); //[6,2,4,5]
    console.log(arr); //[6,2,4,5]
        
4. it either deletes or delete and then replace value from a specified index to no of specified deleting value.

5. Mutates.

**find:**

1. Parameter: callback(element, index, array), this argument
2. Return: The first element in the array that satisfies the provided testing function. Otherwise, undefined is returned.

3. Examples:

    arr = [1,2,3,4,5,6]
    console.log(arr.find(element => element>1)); //2

    arr = [1,2,3,4,5,6]
    console.log(arr.find(element => element<1)); //undefined

    arr = [6,8,10,7,18,20]
    function isOdd(element, index, arr){
        if (element%2 !== 0){
            return element>1;
        }
    }
    console.log(arr.find(isOdd)); //7
        
4. it finds the first element that satified the testing condition, if no element satisfies the testing condition, it returns undefined.

5. No Mutation.

**unshift:**

1. Parameter: element{n}
2. Return: updated length of the array

3. Examples:

    arr = [6,8,10,7,18,20]
    console.log(arr.unshift(1,2)); //8
    console.log(arr); //[1,2,6,8,10,7,18,20]

    arr = [6,8,10,7,18,20]
    console.log(arr.unshift()); //6
    console.log(arr); //[6,8,10,7,18,20]

        
4. it inserts elements at the starting of the array and return length of updated array

5. Mutates.

**findIndex:**

1. Parameter: callback(element, index, array), this argument
2. Return: The index of first element in the array that satisfies the provided testing function. Otherwise, undefined is returned.

3. Examples:

    arr = [1,2,3,4,5,6]
    console.log(arr.find(element => element>1)); //1

    arr = [1,2,3,4,5,6]
    console.log(arr.find(element => element<1)); //undefined

    arr = [6,8,10,7,18,20]
    function isOdd(element, index, arr){
        if (element%2 !== 0){
            return element>1;
        }
    }
    console.log(arr.find(isOdd)); //3
        
4. it finds the index of the first element that satified the testing condition, if no element satisfies the testing condition, it returns undefined.

5. No Mutation.

**filter:**

1. Parameter: callback(element, index, array), this argument
2. Return: an array of elements that satisfied the condition, else empty array is returned.

3. Examples:

    arr = [1,2,3,4,5,6]
    console.log(arr.find(element => element>1)); //[2,3,4,5,6]

    arr = [1,2,3,4,5,6]
    console.log(arr.find(element => element<1)); //[]

    arr = [6,8,10,7,18,20]
    function isOdd(element, index, arr){
        if (element%2 !== 0){
            return element>1;
        }
    }
    console.log(arr.find(isOdd)); //[7]
        
4. it returns a shallow copy of elements that pass the condition in form of an array, if none passed, returns empty array.

5. No Mutation.

**forEach:**

1. Parameter: callback(element, index, array), this argument
2. Return: undefined

3. Examples:

    arr = [1,2,3,4,5,6]
    console.log(arr.forEach(element => console.log(element))); //[1,2,3,4,5,6]

    arr= [1,2,3]
    cparr= []
    arr.forEach(element => cparr.push(element)); 
    console.log(cparr) //[1,2,3]
        
4. it executes the provided function by iterating through each element in the array.

5. No Mutation.

**map:**

1. Parameter: callback(element, index, array), this argument
2. Return: a new array containing transformed value

3. Examples:

    arr = [1,2,3,4,5,6]
    console.log(arr.map(element => element*element)); //[1,4,9,16,25,36]

    arr= [1,2,3]
    arr.forEach(element => element-1); 
    console.log(cparr) //[0,1,2]
        
4. it returns an array containing modified value after transformation in each element of original array.

5. No Mutation.

**pop:**

1. Parameter: none
2. Return: removed element, undefined if array is empty

3. Examples:

    arr = [1,2,3,4,5,6]
    console.log(arr.pop()); //6
    console.log(arr); //[1,2,3,4,5]

    arr= [1,2,3]
    arr.pop(); 
    console.log(arr) //[1,2]
        
4. it removes last element of the array and returns it, if the array is empty it returns undefined.

5. Mutates.

**reduce:**

1. Parameter: callback(accumulator, element, index, array), this argument
2. Return: a number 

3. Examples:

    arr =[1,2,3,4,5,6]
    function sum(sum, element){
        return sum+element;
    }
    console.log(arr.reduce(sum)); //21

    arr =[1,2,3,4,5,6]
    function multiply(sum, element){
        return sum*element;
    }
    console.log(arr.reduce(multiply)); //720

    arr =[1,2,3,4,5,6]
    function max(a,b){
        return a>b ? a:b;
    }
    console.log(arr.reduce(max)); //6
        
4. it returns the final result after the function is ran completely

5. No Mutation.

**slice:**

1. Parameter: start, end optional
2. Return: an array from start to end-1 index of original array, if start>end returns empty array, if start > array.length, returns empty array, by default returns full array

3. Examples:

    arr =[1,2,3,4,5,6]
    console.log(arr.slice()); //[1,2,3,4,5,6]

    arr =[1,2,3,4,5,6]
    console.log(arr.slice(4,2)); /[]

    arr =[1,2,3,4,5,6]
    console.log(arr.slice(1,3)); //[2,3]
        
4. it slices through starting index and end-1 index and returns an array containg required elements

5. No Mutation.

**some:**

1. Parameter: callback(currentElement, currentIndex, array), thisargument optional 
2. Return: true/false

3. Examples:

    const arr1 = [1,2,3,4,5,6,7];

    function big(element, index, array){
        return element>7;
    }
    console.log(arr1.some(big)); //false

    arr =[1,2,3,4,5,6]
    function even(element, index, array){
        return element%2===0;
    }
    console.log(arr.some(even)); //true
    
4. it tests if any element/elements pass the given condition and if any element doesnt pass it return false else return true.

5. No Mutation.
